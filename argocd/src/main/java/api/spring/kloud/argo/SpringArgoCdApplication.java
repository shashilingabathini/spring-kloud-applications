package api.spring.kloud.argo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SpringArgoCdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringArgoCdApplication.class, args);
	}

	@Autowired
	private Configurations configurations;

	@RestController
	@RequestMapping("/v1/api")
	public class APIController {

		@GetMapping(path = "/message/{message}")
		public String message(@PathVariable( name = "message") String message) {
			if(message.equals("shashi")) {
				configurations.setName("new shashi kumar");
			}
			return "Hi ," + message +"  with " +configurations.getName();
		}

		@GetMapping(path = "/message")
		public String message_new() {
			return configurations.getName();
		}


	}


	@Configuration
	@ConfigurationProperties( prefix = "api.messages")
	@Data
	public static class Configurations {
		private String name;
	}

}
