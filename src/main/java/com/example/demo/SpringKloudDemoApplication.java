package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringKloudDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringKloudDemoApplication.class, args);
	}

}
